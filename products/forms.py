from django import forms
from .models import Product, Comment, User
from django.shortcuts import redirect
from django.utils import timezone

#Product model form for use with Product views that need forms
class ProductModelForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = [
            'title',
            'image',
            'description',
            'price'
        ]
        
class CommentModelForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = [
            'content'
            ]
    def form_valid(self, form):
        user = self.request.user
        form.instance.authour = User.objects.get(pk=user)
        form.instance.date = timezone.now()
        return super().form_valid(form)
        