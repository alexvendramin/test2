from django.db import models
from django.contrib.auth.models import User as OriginalUser

# For simplicity in authentication I extended the User clas from
# django.contrib.auth.models to hold the information in a one to one
# relationship as described in the django description.
class User(models.Model):
    user = models.OneToOneField(OriginalUser, on_delete=models.CASCADE, primary_key=True)
    account_balance = models.DecimalField(max_digits=10, decimal_places=2)
    avatar = models.ImageField("media/", default=None)
    