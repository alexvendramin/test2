from django.contrib.auth import views as auth_views
from django.urls import path
from .views import register_view, UserUpdateView, OriginalUserUpdateView
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static

#urls that are used with the various views
app_name = 'users'
urlpatterns = [
    path('register/', register_view, name='register_view'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
    path('<int:pk>/update/avatar', UserUpdateView.as_view(template_name='users/user_update.html'), name='user_update'),
    path('<int:pk>/update/', OriginalUserUpdateView.as_view(template_name='users/user_update.html'), name='user_update'),
    path('change-pw/', auth_views.PasswordChangeView.as_view(success_url='/',template_name='users/change_password.html'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

