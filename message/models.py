from django.db import models

# Create your models here.
class Blog(models.Model):
    creation_date = models.DateTimeField()
    last_modification = models.DateTimeField()
    

class Entry(models.Model):
    blog = models.ForeignKey('Blog', on_delete=models.CASCADE)
    authour = models.TextField()
    entry_date = models.DateTimeField()