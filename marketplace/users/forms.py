from django import forms
from django.contrib.auth.models import User as OriginalUser
from .models import User
from django.contrib.auth.forms import UserCreationForm

#form with fields for a user made for the register functionality
class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=30)
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField(max_length=200)
    
    class Meta:
        model = OriginalUser
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')

#basic user model form with avatar for add avatar funcionality
class UserModelForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'avatar'
        ]
        
    #basic user model form with avatar for add avatar funcionality
class OriginalUserModelForm(forms.ModelForm):
    class Meta:
        model = OriginalUser
        fields = [
            'username',
            'first_name',
            'last_name',
            'email'
        ]