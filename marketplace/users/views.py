from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from .forms import SignUpForm, UserModelForm, OriginalUserModelForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User as OriginalUser
from .models import User
from django.shortcuts import redirect
from django.views.generic import UpdateView

#view for registering that creates a new user based on info from the form
def register_view(request):
    form = SignUpForm(request.POST)
    if form.is_valid():
        form.save()
        username = form.cleaned_data.get('username')
        first_name = form.cleaned_data.get('first_name')
        last_name = form.cleaned_data.get('last_name')
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password1')
        user = authenticate(username=username, password=password)
        new_user = User(user=user, account_balance=0)
        new_user.save()
        login(request,user)
        return redirect('/products/')
    return render(request, 'users/register.html', {'form':form})

#view to support changing avatar
class UserUpdateView(LoginRequiredMixin, UpdateView):
    success_url="/"
    form_class = UserModelForm
    queryset = User.objects.all();
    
#view to support changing user info
class OriginalUserUpdateView(LoginRequiredMixin, UpdateView):
    success_url="/"
    form_class = OriginalUserModelForm
    queryset = User.objects.all();
    