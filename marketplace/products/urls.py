from django.urls import path, re_path
from .views import ProductListView, ProductDetailView, ProductCreateView, ProductUpdateView, ProductDeleteView, buy_view, CommentCreateView
from django.conf import settings
from django.conf.urls.static import static

#urls that are used with the various views
#many variations of products/<int:pk>/ which just describes a uniques url for each item in the database
app_name = 'products'
urlpatterns = [
    path('', ProductListView.as_view(), name='product-list'),
    path('<int:pk>/', ProductDetailView, name='product-detail'),
    path('create/', ProductCreateView.as_view(template_name='products/product_create.html'), name='product_create'),
    path('comments/create/', CommentCreateView.as_view(template_name='products/product_create.html'), name='comment_create'),
    path('<int:pk>/update/', ProductUpdateView.as_view(template_name='products/product_update.html'), name='product_update'),
    path('<int:pk>/delete/', ProductDeleteView.as_view(template_name='products/product_delete.html'), name='product_delete'),
    path('<int:pk>/buy/', buy_view, name='product_buy')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)