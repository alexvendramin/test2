from django.db import models
from users.models import User
from django.shortcuts import redirect

# Create your models here.
#Product model has fields representative of a product sold on the site
class Product(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.TextField()
    image = models.ImageField("media/")
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    
    
    def get_absolute_url(self):
        return f"/products/{self.id}/"
    
class Comment(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, default=None)
    content = models.TextField()
    authour = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    date = models.DateTimeField()
    
    def get_absolute_url(self):
        return f"/products/{self.product.id}/"