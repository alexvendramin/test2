from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
from django.shortcuts import render, redirect
from .models import User, Comment
from .forms import CommentModelForm
from django.views.generic import (
    CreateView,
    UpdateView,
    DeleteView,
    ListView,
    DetailView
)
#ALL CLASSES WHICH TAKE LoginRequiredMixin CAN ONLY BE ACCESSED IF YOU ARE LOGGED IN
from .models import Product
from .forms import ProductModelForm
#overrides form_valid and omits owner from fields so that 
#it's value is always the user themself i.e. they can't
#add product for somone else
class ProductCreateView(LoginRequiredMixin, CreateView):
    model = Product
    fields = [
        'title',
        'image',
        'description',
        'price'
    ]
    def form_valid(self, form):
        user = self.request.user
        form.instance.owner = User.objects.get(pk=user)
        return super().form_valid(form)
#similar to above it's a comment creation with some values
#that are not determined by the user
class CommentCreateView(LoginRequiredMixin, CreateView):
    model = Comment
    fields = [
            'content'
    ]
    def form_valid(self, form):
        user = self.request.user
        form.instance.product = Product.objects.get(id=self.request.GET['id'])
        form.instance.authour = User.objects.get(pk=user)
        form.instance.date = timezone.now()
        return super().form_valid(form)

#very basic update view using imported UpdateView
class ProductUpdateView(LoginRequiredMixin, UpdateView):
    form_class = ProductModelForm
    queryset = Product.objects.all()
#primitive function based view for the index
def index_view(request, *args, **kwargs):
    return render(request, "index.html", {})
#handles buying an item from the product-details page and 
#makes sure that the money is handled correctly
def buy_view(request, *args, **kwargs):
    user = request.user
    print(user)
    buyer = User.objects.get(user=user)
    print(buyer)
    item = Product.objects.get(id=kwargs['pk'])
    seller = item.owner
    if buyer.account_balance >= item.price:
        print(seller.account_balance)
        seller.account_balance = seller.account_balance+item.price
        buyer.account_balance = buyer.account_balance-item.price
        seller.save()
        buyer.save()
        item.delete()
    else:
        return render(request, "products/buy_failed.html", {})
    
    return render(request, "index.html", {})
#basic list view which can dynamically filter the querysey based
#on the url parameters fed by a form in the template
class ProductListView(ListView):
    def get_queryset(self):
        filter = self.request.GET.get('filter', 'id')
        query = self.request.GET.get('q', '')
        if filter == 'owned':
            user = User.objects.get(user=self.request.user)
            qs = Product.objects.filter(owner=user)
            return qs.filter(title__icontains=query)
        return Product.objects.filter(title__icontains=query).order_by(filter)
#detail view which allows comments to be rendered with the object   
def ProductDetailView(request, pk):
    template_name = 'products/product_detail.html'
    product = Product.objects.get(id=pk)
    comments = product.comment_set.all()
    return render(request, template_name, {'object': product, 'comments': comments})    
    
#basic delete view which uses the imported DeleteView
class ProductDeleteView(LoginRequiredMixin, DeleteView):
    queryset = Product.objects.all();
    success_url = '/products'
    