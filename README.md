# 420-420-team8
Mishal Alarifi and Alex Vendramin
Our project of course is the ecommerce project similar to kijij called klone
you can run it from heroku or if that is not working you can pull from our git
repo here: https://gitlab.com/ma.alarifi/420-420-team8.git and open an anaconda
shell in the repo and run python manage.py runserver and then connect to
localhost on port 8000 in a browser.

The Heroku link is: https://dry-inlet-36971.herokuapp.com

You can access the admin page with a button ONLY if you are logged in as a super user
however to make it more convenient for you to grade you can still connect to the link
manually.

The usernames of each account are as follows: Carlover, Gamelover, Sportlover, Clotheslover, Booklover, Admin, Dev
ALL of the passwords are the same: Romewasbuilton7hills

Something to note is that we made it so that visitors are unauthourised 
users and so they are NOT stored in the database.